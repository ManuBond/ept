﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsSpecs : MonoBehaviour {

    public float Damage, FireRate;
    public int Ammo;
    public Sprite BulletSprite;
}
