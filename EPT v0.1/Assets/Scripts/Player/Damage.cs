﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{

	public AudioClip sonido = null;
    public float DuracionAtaque;
    public float Daño;
    public GameObject Autor;
    private Animator ane;

    // Cada vez que se abilite el objeto que tenga este script (HitRange's tanto de jugador como de enemigos melee) correra el metodo que lo volvera a desactivar
    private void OnEnable()
    {
		if (sonido)
        {
            AudioSource.PlayClipAtPoint(sonido, transform.position, 25);
        }
        Invoke("Desactivar", DuracionAtaque);
    }

    void Desactivar()
    {
        Animator an = Autor.GetComponent<Animator>();
        an.SetBool("Attack", false);
        gameObject.SetActive(false);
    }

    // Al haber algun enemigo dentro del area de golpe, este recibira daño
    private void OnTriggerEnter(Collider other)
    {
        //Daño
        if (other.GetComponent<VidaEnemigos>() != null)
        {
            other.GetComponent<VidaEnemigos>().Cantidad -= Daño;
            //Recoil
            if (other.GetComponent<VidaEnemigos>().Muerto == false && other.transform.position.x > -8.35f /*Falta maximo permitido por el stage*/)
            {
                if (transform.position.x > other.transform.position.x)
                {
                    other.transform.Translate(new Vector3(-0.1f, 0, 0));
                }
                if (transform.position.x < other.transform.position.x)
                {
                    other.transform.Translate(new Vector3(0.1f, 0, 0));
                }
            }
            //Animacion
            if (other.GetComponentInChildren<Animator>() != null)
            {
                ane = other.GetComponentInChildren<Animator>();
                ane.SetBool("DañoRecibido", true);
                Invoke("CancelarAnimacion", 0.5f);
            }
        }
    }

    void CancelarAnimacion()
    {
        ane.SetBool("DañoRecibido", false);
    }
}
