﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vida : MonoBehaviour {

	public float volumen;
	public float Cantidad;
    public bool Muerto;
    private Animator an;
	public GameObject Player, sonidofondo, LoserText;
    public AudioClip sonido = null;
	public Button reintentar;
	public Button salir;
	public Button menu;

    private void Awake()
    {
        an = Player.GetComponent<Animator>();
    }

    private void Update()
    {
        if (Cantidad <= 0)
        {
            an.SetBool("Dead", true);
            GetComponent<CC>().Gun.SetActive(false);
            GetComponent<CC>().enabled = false;

			sonidofondo.SetActive(false);
			if(sonido != null && Muerto == false) {
                Muerto = true;
				AudioSource.PlayClipAtPoint(sonido,gameObject.transform.position,volumen);
				reintentar.gameObject.SetActive (true);
				menu.gameObject.SetActive (true);
				salir.gameObject.SetActive (true);
			}
            LoserText.SetActive(true);
		}
    }
}
