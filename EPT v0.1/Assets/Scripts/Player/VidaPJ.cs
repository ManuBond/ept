﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaPJ : MonoBehaviour 
{
	public Text Life;
	public GameObject PJ;
	public float vida;
	public static GameObject cora;
	public GameObject corazon;
	public Animator anim;

	void Awake()
	{
		anim = corazon.GetComponent<Animator>();

	}

	void Update()
	{
		cora = corazon;
		PJ = GameObject.Find ("Player");
		vida = PJ.GetComponent<Vida> ().Cantidad;
        if (vida > 0)
        {
            Life.text = vida.ToString();
        }
        else
        {
            Life.text = "0";
        }

		if (vida <= 0) 
		{
			anim.SetBool ("MUERE", true);
			anim.SetBool ("SIN CORAZON", true);
		}
	}

}
