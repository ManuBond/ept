﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectile : MonoBehaviour {

    public float Vel, Daño, Destruccion;
    private Animator ane;
	public GameObject sangre, clone, Gun;
	public AudioClip sonidoDisparo;

    private void Awake()
    {
		AudioSource.PlayClipAtPoint (sonidoDisparo, transform.position);
        Invoke("Destruir", Destruccion);
        //parti = Instantiate (fuego, transform.position, new Quaternion ());
        //Destroy (parti.gameObject, 1);
        Gun = GameObject.FindGameObjectWithTag("Equipped");
        GetComponent<SpriteRenderer>().sprite = Gun.GetComponent<WeaponsSpecs>().BulletSprite;

    }

    private void FixedUpdate()
    {
        transform.Translate(Vel, 0, 0);
    }

    private void OnTriggerEnter(Collider other)
    { 
		//Daño
        if (other.GetComponent<VidaEnemigos>() != null)
        {
            //Particulas de sangre
            clone = Instantiate(sangre, transform.position, new Quaternion());
            Destroy(clone.gameObject, 1);

            //Vector3 Posicion = new Vector3 (gameObject.transform.position.x,gameObject.transform.position.y,gameObject.transform.position.z);
				
			other.GetComponent<VidaEnemigos>().Cantidad -= Gun.GetComponent<WeaponsSpecs>().Damage;

			//Recoil
            if (other.GetComponent<VidaEnemigos>().Muerto == false && other.transform.position.x > -8.30f && other.transform.position.x < 164.32f)
            {
                if (transform.position.x > other.transform.position.x)
                {
                    other.transform.Translate(new Vector3(-0.5f, 0, 0));
                }
                if (transform.position.x < other.transform.position.x)
                {
                    other.transform.Translate(new Vector3(0.5f, 0, 0));
                }
            }
            //Animacion
            if (other.GetComponentInChildren<Animator>() != null)
            {
                ane = other.GetComponentInChildren<Animator>();
                ane.SetBool("DañoRecibido", true);
            }
            Invoke("Destruir", 0);
        }
    }

    void Destruir()
    {
        Destroy(gameObject);
    }
}
