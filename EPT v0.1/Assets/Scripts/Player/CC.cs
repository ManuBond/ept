﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CC : MonoBehaviour {

    private CharacterController cc;
    private Animator an, gunan;
    public GameObject Character, Camara, Gun, Bullet, OriginalGun, BulletText, Infinity, CanvasBullet, DevText, DevTools, BackBuildings;
    public bool muerto, ataquePermitido = true, armaEspecial, DevMode;
    public float Velocidad;
    public int CurrentSpecialAmmo;

    private void Awake()
    {
        Gun = OriginalGun;
        cc = GetComponent<CharacterController>();
        an = Character.GetComponent<Animator>();
        gunan = Gun.GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        /**************************************************** DEVELOPER MODE ****************************************************/
                                                    if (Input.GetKey(KeyCode.L) == true)
                                                    {
                                                        DevMode = !DevMode;
                                                        DevText.SetActive(DevMode);
                                                        if (DevMode == true)
                                                        {
                                                            Velocidad = 8;
                                                            OriginalGun.GetComponent<WeaponsSpecs>().Damage = 1000;
                                                            GetComponentInChildren<Vida>().Cantidad = 999;
                                                            DevTools.SetActive(true);
                                                        }
                                                        else
                                                        {
                                                            Velocidad = 5;
                                                            OriginalGun.GetComponent<WeaponsSpecs>().Damage = 50;
                                                            GetComponentInChildren<Vida>().Cantidad = 100;
                                                            DevTools.SetActive(false);
                                                        }
                                                    }
        /************************************************** END DEVELOPER MODE **************************************************/

        an.SetBool("DañoRecibido", false);
        muerto = GetComponentInChildren<Vida>().Muerto;
        Vector3 mov = Vector3.zero;
                                                                  /*MOVIMIENTO*/
        /* MOVIMIENTO VERTICAL (eje Z) */

        // Movimiento hacia adelante cuando se incrementa el Axis Vertical y hacia atras cuando el Axis Vertical es negativo
        mov += transform.forward * Input.GetAxis("Vertical") * Velocidad;

        /* MOVIMIENTO LATERAL (eje X) */

        // Movimiento hacia la derecha cuando se incrementa el Axis Horizontal y hacia la izquierda cuando el Axis Horizontal es negativo
        mov += transform.right * Input.GetAxis("Horizontal") * Velocidad;

        // Todos los cambios efectuados en los ejes gracias a los "GetAxis" se implementan en el CharacterController haciendo que el personaje se mueva
        if (muerto == false)
        {
            cc.Move(mov * Time.deltaTime);
            transform.SetPositionAndRotation((new Vector3(transform.position.x, 2, transform.position.z)), transform.rotation);

            // El personaje rota sobre su eje Y para mirar hacia la izquierda o derecha
            if (Input.GetAxis("Horizontal") < 0)
            {
                Character.transform.eulerAngles = new Vector3(-35, 180, 0);
            }
            if (Input.GetAxis("Horizontal") > 0)
            {
                Character.transform.eulerAngles = new Vector3(35, 0, 0);
            }
        }

        // Esto evita que la camara se mueva mas alla de donde deberia para evitar que se vea el extremo del mapa
        if (Character.transform.position.x >= 0 && Character.transform.position.x <= 156)
        {
            Camara.transform.SetPositionAndRotation((new Vector3(gameObject.transform.position.x, Camara.transform.position.y, Camara.transform.position.z)), Camara.transform.rotation);
            BackBuildings.transform.Translate((mov.x * (-0.0015f)), (mov.y * (-0.02f)), (mov.z * (-0.02f)));
        }

        /*GOLPE*/
        if (muerto == false && ataquePermitido == true)
        {
            if (Input.GetKey(KeyCode.Space) == true)
            {
                ataquePermitido = false;
                an.SetBool("Attack", true);
                gunan.SetBool("Fire", true);
                Invoke("CancelarAnimacion", Gun.GetComponent<WeaponsSpecs>().FireRate);

                Instantiate(Bullet, Gun.transform.position, Gun.transform.rotation);

                if (armaEspecial == true)
                {
                    CurrentSpecialAmmo--;

                    BulletText.GetComponent<Text>().text = CurrentSpecialAmmo + "/" + Gun.GetComponent<WeaponsSpecs>().Ammo.ToString();

                    if (CurrentSpecialAmmo <= 0)
                    {
                        Destroy(Gun);
                        armaEspecial = false;
                        Gun = OriginalGun;
                        CanvasBullet.GetComponent<Image>().sprite = Gun.GetComponent<WeaponsSpecs>().BulletSprite;
                        BulletText.SetActive(false);
                        Infinity.SetActive(true);
                        gunan = Gun.GetComponent<Animator>();
                        gunan.enabled = true;
                        OriginalGun.SetActive(true);
                    }
                }
            }
        }

                                                                  /*ANIMACION*/
        // Animacion de caminata
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            an.SetFloat("Vel", 1);
        }
        else
        {
            an.SetFloat("Vel", 0);
        }
    }

    void CancelarAnimacion()
    {
        gunan.SetBool("Fire", false);
        an.SetBool("Attack", false);
        ataquePermitido = true;
    }

    /* Recoleccion de armas */
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Arma")
        {
            if (Input.GetKey(KeyCode.F) == true)
            {
                if (armaEspecial == true)
                {
                    Destroy(Gun);
                    armaEspecial = false;
                }
                GameObject newgun = other.gameObject;
                Destroy(other.gameObject);
                (Gun = (Instantiate(newgun, OriginalGun.transform.position, OriginalGun.transform.rotation) as GameObject)).transform.parent = Character.transform;
                OriginalGun.SetActive(false);
                Gun.transform.GetChild(0).gameObject.SetActive(false);
                Gun.GetComponent<SpriteRenderer>().sortingLayerName = "EquippedWeapons";
                gunan = Gun.GetComponent<Animator>();
                gunan.enabled = true;
                CanvasBullet.GetComponent<Image>().sprite = Gun.GetComponent<WeaponsSpecs>().BulletSprite;
                CurrentSpecialAmmo = Gun.GetComponent<WeaponsSpecs>().Ammo;
                Infinity.SetActive(false);
                BulletText.GetComponent<Text>().text = CurrentSpecialAmmo + "/" + Gun.GetComponent<WeaponsSpecs>().Ammo.ToString();
                BulletText.SetActive(true);
                armaEspecial = true;
                Gun.tag = "Equipped";
            }
        }
    }
}
