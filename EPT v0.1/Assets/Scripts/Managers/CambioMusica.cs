﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioMusica : MonoBehaviour {

	public AudioSource audio;
	public AudioSource reemplazo;


	void Update () {

		if (EnemyManager.EnemigosMatados == EnemyManager.MaximoEnemigosEnNivel) 
		{
			audio.gameObject.SetActive (false);
			reemplazo.gameObject.SetActive (true);
		}	
	}
}
