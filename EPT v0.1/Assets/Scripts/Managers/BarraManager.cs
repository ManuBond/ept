﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraManager : MonoBehaviour {

	public Slider Barra;
	public Image imagen;
	public GameObject Boss;
	public float BossTotalLife;

	void Update()
	{
		if (GameObject.Find ("Frank(Clone)") != null)
        {
            Boss = GameObject.Find("Frank(Clone)");
            Barra.gameObject.SetActive (false);
			imagen.gameObject.SetActive (true);
            imagen.fillAmount = (Boss.GetComponentInChildren<VidaEnemigos>().Cantidad / BossTotalLife);
		} 
	}
}
