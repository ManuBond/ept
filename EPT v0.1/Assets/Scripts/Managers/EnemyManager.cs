﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public static EnemyManager instance;

    public static int EnemigosMatados;
	public static int MaximoEnemigosEnNivel = 30;
    public static bool Boss;
    public GameObject Player, Camera, Frank, Enemigos;
    private CharacterController cc;
    private CC ccscript;

    private void Awake()
    {
        instance = this; //Se lo convierte en Manager
        cc = Player.GetComponent<CharacterController>();
        ccscript = Player.GetComponent<CC>();
        EnemigosMatados = 0;
    }

    private void Update()
    {
        if (Boss == false)
        {
            if (EnemigosMatados == MaximoEnemigosEnNivel)
            {    //Al llegar al maximo de enemigos matados por primera vez se inicia la BossFight:
                cc.enabled = false;
                ccscript.enabled = false;
                if (Camera.transform.position.x < 180)
                {
                    Camera.transform.Translate(0.5f, 0, 0);
                }
                if (Player.transform.position.x < 175)
                {
                    Player.transform.Translate(0.3f, 0, 0);
                }
            }
            if (Player.transform.position.x >= 175 && Camera.transform.position.x >= 180)
            {
                Camera.transform.SetPositionAndRotation(new Vector3(180, Camera.transform.position.y, Camera.transform.position.z), Camera.transform.rotation);
                Player.transform.SetPositionAndRotation(new Vector3(175, Player.transform.position.y, Player.transform.position.z), Player.transform.rotation);
                cc.enabled = true;
                ccscript.enabled = true;
                Enemigos.SetActive(false);
                Instantiate(Frank);
                Boss = true;
            }
        }
    }
}
