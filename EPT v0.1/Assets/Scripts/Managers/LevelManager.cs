﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    public static LevelManager instance;

    public GameObject Player, Character, Gun, Frank, WinnerText;
    private CharacterController cc;
    private CC ccscript;
    private Animator an;
    public static bool BossFinding, BossFight, BossDefeated;
    public Button boton;

    private void Awake()
    {
        instance = this; //Se lo convierte en Manager
        cc = Player.GetComponent<CharacterController>();
        ccscript = Player.GetComponent<CC>();
        an = Player.GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (GameObject.Find("Frank(Clone)") != null && BossFinding == false)
        {
            Frank = GameObject.Find("Frank(Clone)");
            BossFinding = true;
            BossFight = true;
        }
        if (BossFight == true && Frank != null)
        {
            if (Frank.GetComponentInChildren<VidaEnemigos>().Cantidad <= 0)
            {
                Gun = Player.GetComponent<CC>().Gun;
                BossFight = false;
                cc.enabled = false;
                ccscript.enabled = false;
                Gun.SetActive(false);
                Character.transform.eulerAngles = new Vector3(35, 0, 0);
                an.SetBool("Winner", true);
                Invoke("CancelarAnimacion", 5);
            }
        }
        if (BossDefeated == true)
        {
            if (Player.transform.position.x < 200)
            {
                Invoke("ProximoNivel", 0);
            }
        }
    }

    void CancelarAnimacion()
    {
        an.SetBool("Winner", false);
        an.SetFloat("Vel", 1);
        BossDefeated = true;
    }

    void ProximoNivel()
    {
        if (Player.transform.position.x < 200)
        {
            Player.transform.Translate(0.05f, 0, 0);
        }
        if (Player.transform.position.x > 195)
        {
            WinnerText.SetActive(true);
            boton.gameObject.SetActive(true);
        }
    }
}
