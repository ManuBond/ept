﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraProgreso : MonoBehaviour {

	Slider Barra;

	public float max = EnemyManager.MaximoEnemigosEnNivel;
	public float act;

	void Awake()
	{
		Barra = GetComponent<Slider> ();

	}

	void Update()
	{
		act = EnemyManager.EnemigosMatados;

		ActualizarValorBarra (max, act);
	}

	void ActualizarValorBarra(float valorMax,float Valoract)
	{
		float porcentaje;
		porcentaje = Valoract / valorMax;
		Barra.value = porcentaje;
	}
}
