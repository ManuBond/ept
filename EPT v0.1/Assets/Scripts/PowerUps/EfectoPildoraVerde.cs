﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfectoPildoraVerde : MonoBehaviour {

	public GameObject animacion, clone;
	private GameObject pj;
    private bool EraEspecial;

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Player")
        {
            EraEspecial = col.GetComponent<CC>().armaEspecial;
            col.GetComponent<CC>().Gun.GetComponent<WeaponsSpecs>().Damage *= 2;
		    pj = col.gameObject;
            Invoke ("Cancel", 10f);
            GetComponent<SphereCollider>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            //clone = Instantiate (animacion, transform.position, new Quaternion());
            //Destroy (clone.gameObject, 1);
        }
    }

	void Cancel()
	{
        if (EraEspecial = pj.GetComponent<CC>().armaEspecial) // FALTA ARREGLAR ESTO, MANU LO HARA LUEGO
        {
            pj.GetComponent<CC>().Gun.GetComponent<WeaponsSpecs>().Damage /= 2;
        }
        Destroy(gameObject);
    }
}
