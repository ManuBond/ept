﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfectoPildoraAzul : MonoBehaviour {

	public GameObject animacion, clone;
	private GameObject pj;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player")
        {
			col.GetComponent<CC>().Velocidad *= 1.3f;
			pj = col.gameObject;
			Invoke ("Cancel", 10f);
            GetComponent<SphereCollider>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            //clone = Instantiate (animacion, transform.position, new Quaternion()); 
            //Destroy (clone.gameObject, 1);
        }
	}

	void Cancel()
	{
		pj.GetComponent<CC>().Velocidad /= 1.3f;
        Destroy(gameObject);
	}
}
