﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfectoPilRoja : MonoBehaviour {

	public GameObject animacion;
	GameObject clone;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			Destroy (gameObject);
			col.gameObject.GetComponent<Vida> ().Cantidad += 20;
			clone = Instantiate (animacion, transform.position, new Quaternion());
			Destroy (clone.gameObject, 1);
		}
	}
}
