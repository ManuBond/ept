﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBall : MonoBehaviour {

    public Transform PJ;
    public Animator anp;
    public float Vel, PlayersXLastKnownLocation, PlayersZLastKnownLocation, TimerBomb, Daño;
    private bool SpecialAttack, Subio;

    private void Awake()
    {
        PJ = GameObject.Find("Player").transform;
        PlayersXLastKnownLocation = PJ.position.x;
        PlayersZLastKnownLocation = PJ.position.z;
        Invoke("AutoDestroy", TimerBomb);
    }

    private void Update()
    {
        if (SpecialAttack == false)
        {
            //Eje vertical
            if (gameObject.transform.position.y < 7 && Subio == false) //Sube
            {
                gameObject.transform.Translate(0, Vel, 0);
            }
            else
            {
                Subio = true;
            }
            if (gameObject.transform.position.y > 2 && Subio == true) //Baja
            {
                gameObject.transform.Translate(0, -Vel, 0);
            }

            //Eje horizontal
            if (gameObject.transform.position.x > PlayersXLastKnownLocation)
            {
                gameObject.transform.Translate(-Vel, 0, 0);
            }
            if (gameObject.transform.position.x < PlayersXLastKnownLocation)
            {
                gameObject.transform.Translate(Vel, 0, 0);
            }

            //Eje de profundidad
            if (gameObject.transform.position.z > PlayersZLastKnownLocation)
            {
                gameObject.transform.Translate(0, 0, -Vel);
            }
            if (gameObject.transform.position.z < PlayersZLastKnownLocation)
            {
                gameObject.transform.Translate(0, 0, Vel);
            }
        }
        else // SpecialAttack == true
        {
            //Para el lvl 1
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Daño
        if (other.GetComponent<Vida>() != null)
        {
            other.GetComponent<Vida>().Cantidad -= Daño;
            //Animacion
            if (other.GetComponentInChildren<Animator>() != null)
            {
                anp = other.GetComponentInChildren<Animator>();
                anp.SetBool("DañoRecibido", true);
            }
            Invoke("AutoDestroy", 0);
        }
    }

    void AutoDestroy()
    {
        Destroy(gameObject);
    }
}
