﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FranksBehaviour : MonoBehaviour
{
    private NavMeshAgent nma;
    public Transform[] targets;
    private int currentWaypointF, BallTurn;
    private Animator anF;
    public GameObject SpriteF, PJF, Ball, SpecialBall;
    private bool AttackAvaliable, WasteingTimeBool;

    void Awake()
    {
        nma = GetComponent<NavMeshAgent> ();
        nma.destination = targets[currentWaypointF].position;
        anF = SpriteF.GetComponent<Animator>();
        // Esta linea hace que se le asigne la posicion del jugador al crear la instancia del prefab, de otra forma no se puede
        PJF = GameObject.Find("Player");
        GetComponent<VidaEnemigos>().boss = true;
    }

    private void FixedUpdate()
	{
		if (GameObject.Find ("Frank(Clone)") != null)
        {
            SpriteF.transform.eulerAngles = new Vector3(35, 0, 0);
            if (!nma.pathPending && nma.remainingDistance <= nma.stoppingDistance) { //Ruta de movimiento
				//0 % 5 = 0
				//1 % 5 = 1
				//2 % 5 = 2
				//3 % 5 = 3
				//4 % 5 = 4
				//5 % 5 = 0
				//if (currentWaypoint >= targets.Length) currentWaypoint = 0;

				BallTurn = currentWaypointF;

				currentWaypointF++;
				currentWaypointF %= targets.Length;

				DoAttack ();

			}

			/* ANIMACION */
			if (nma.hasPath == true) {
				anF.SetFloat ("Vel", 1);
			} else {
				anF.SetFloat ("Vel", 0);
			}
		}
	}

    private void DoAttack()
    {
        AttackAvaliable = false;
        anF.SetBool("Attack", true);
        Invoke("Attack", 0.78f);
    }

    void Attack()
    {
        /* ATAQUE DE FRANK */
        if (WasteingTimeBool == true)
        {
            if (BallTurn < 3 && AttackAvaliable == false)
            {
                AttackAvaliable = true;
                Instantiate(Ball, transform.position, transform.rotation);
                WasteingTimeBool = !WasteingTimeBool;
            }
            if (BallTurn == 3 && AttackAvaliable == false)
            {
                AttackAvaliable = true;
                Instantiate(SpecialBall, transform.position, transform.rotation);
                WasteingTimeBool = !WasteingTimeBool;
            }

            anF.SetBool("Attack", false);
            nma.destination = targets[currentWaypointF].position;
        }
        WasteingTimeBool = !WasteingTimeBool;
    }
}