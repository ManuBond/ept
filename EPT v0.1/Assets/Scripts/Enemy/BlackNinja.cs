﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BlackNinja : MonoBehaviour {

	//private int currentWaypoint;
	//private NavMeshAgent nma;
	public GameObject[] PowerUps;
	public Transform PJ;
    public GameObject HitRange, Sprite;
	public float vel, AttackDelay;
    private bool muerto;
    private Animator an;

    void Awake()
	{
		//nma = GetComponent<NavMeshAgent> ();
		//nma.destination = targets[currentWaypoint].position;
        an = Sprite.GetComponent<Animator>();

        // Esta linea hace que se le asigne la posicion del jugador al crear la instancia del prefab, de otra forma no se puede
        PJ = GameObject.Find("Player").transform;
    }

    void Update()
	{
        an.SetBool("DañoRecibido", false);
        muerto = GetComponent<VidaEnemigos>().Muerto;
        if (muerto == false)
        {
            if (PJ)
            {

                Vector3 Adelante = transform.TransformDirection(Vector3.right);
                Vector3 Personaje = PJ.position - transform.position;
                float distancia = Vector3.Distance(transform.position, PJ.position);

                Vector3 direccion = Vector3.Normalize(Personaje);
                Vector3 mov = direccion * vel * Time.deltaTime;
                mov = Vector3.ClampMagnitude(mov, distancia);

                if (((Vector3.Dot(Adelante, Personaje) < 1.6f) || (Vector3.Dot(Adelante, Personaje) > (-1.6f))) && (PJ.position.x > (transform.position.x - 15))) //Lo sigue
                {
                    if (distancia > 1)
                    {
                        transform.position += mov;
                        an.SetFloat("Vel", 1);
                    }
                    else
                    {
                        // Aca se invoca al ataque para darle un "Delay", sino el jugador muere muy rapido
                        Invoke("Attack", AttackDelay);
                        an.SetFloat("Vel", 0);
                    }
                }
                /* YA QUE LOS ENEMIGOS NO PATRULLAN, ESTO ES INNECESARIO.

                 * Se va a mantener comentado por ahora para hacer uso de este pedazo de codigo en otro script.

                else //Patrulla
                {

                    if (!nma.pathPending && nma.remainingDistance <= nma.stoppingDistance)
                    {
                        //0 % 5 = 0
                        //1 % 5 = 1
                        //2 % 5 = 2
                        //3 % 5 = 3
                        //4 % 5 = 4
                        //5 % 5 = 0
                        //if (currentWaypoint >= targets.Length) currentWaypoint = 0;

                        currentWaypoint++;
                        currentWaypoint %= targets.Length;
                        nma.destination = targets[currentWaypoint].position;
                    }
                }
                */

                /*ROTACION*/
                if (PJ.position.x > Sprite.transform.position.x)
                {
                    Sprite.transform.eulerAngles = new Vector3(-35, 180, 0);
                }
                else
                {
                    Sprite.transform.eulerAngles = new Vector3(35, 0, 0);
                }
            }
        }
        else // Cuando muere
        {
            if (an.GetBool("Muerto") == false)
            {
                GetComponent<CapsuleCollider>().enabled = false;
                int rnd = Random.Range(1, 100);
                an.SetInteger("RandomDie", rnd);
                an.SetBool("Muerto", true);
                int chance = Random.Range(1, 100);
                if (chance <= 10)
                {
                    Invoke("Loot", 1.3f);
                }
            }
        }
	}

    void Attack()
    {
        // Esto es lo que efectivamente lleva a cabo el ataque, al activar este objeto se corre el script "Damage"
        HitRange.SetActive(true);
        an.SetBool("Attack", true);
    }

    void Loot()
    {
		int pills = Random.Range(0, (PowerUps.Length));
		Instantiate((PowerUps[pills]), gameObject.transform.position, gameObject.transform.rotation);
    }
}
