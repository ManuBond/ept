﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaEnemigos : MonoBehaviour {

    public float Cantidad;
    public bool Muerto;
    public bool boss;

    private void Update()
    {
        if (Cantidad <= 0 && Muerto == false)
        {
            Muerto = true;
            if (boss == false)
            {
                Invoke("Desaparecer", 10);
                EnemyManager.EnemigosMatados++;
            }
            else
            {
                GetComponent<FranksBehaviour>().enabled = false;
                GetComponentInChildren<Animator>().SetBool("Dead", true);
            }
        }
    }

    void Desaparecer()
    {
        Destroy(gameObject);
    }
}
