﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProyectile : MonoBehaviour
{

    public float Vel, Daño, Destruccion;
    private Animator anp;

    private void Awake()
    {
        Invoke("Destruir", Destruccion);
    }

    private void FixedUpdate()
    {
        transform.Translate(-Vel, 0, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Daño
        if (other.GetComponent<Vida>() != null)
        {
            other.GetComponent<Vida>().Cantidad -= Daño;
            //Recoil
            if (other.GetComponent<Vida>().Muerto == false && other.transform.position.x > -8.30f && other.transform.position.x < 164.32f)
            {
                if (transform.position.x > other.transform.position.x)
                {
                    other.transform.Translate(new Vector3(-0.5f, 0, 0));
                }
                if (transform.position.x < other.transform.position.x)
                {
                    other.transform.Translate(new Vector3(0.5f, 0, 0));
                }
            }
            //Animacion
            if (other.GetComponentInChildren<Animator>() != null)
            {
                anp = other.GetComponentInChildren<Animator>();
                anp.SetBool("DañoRecibido", true);
            }
            Invoke("Destruir", 0);
        }
    }

    void Destruir()
    {
        Destroy(gameObject);
    }
}
