﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteNinjaBehaviour : MonoBehaviour {

    public GameObject Sprite, Gun, Bullet;
    public GameObject[] LootGuns;
    public Transform PJ;
    private Animator an, gunan;
    public float vel, AttackDelay;
    private bool muerto, ataquePermitido = true;

    void Awake()
    {
        an = Sprite.GetComponent<Animator>();
        gunan = Gun.GetComponent<Animator>();
        PJ = GameObject.Find("Player").transform;
    }

    void Update ()
    {
        an.SetBool("DañoRecibido", false);
        muerto = GetComponent<VidaEnemigos>().Muerto;
        if (muerto == false)
        {
            if (PJ)
            {
                //Deteccion del jugador y acercamiento
                Vector3 Adelante = transform.TransformDirection(Vector3.right);
                Vector3 Personaje = PJ.position - transform.position;
                float distancia = Vector3.Distance(transform.position, PJ.position);

                Vector3 direccion = Vector3.Normalize(Personaje);
                Vector3 mov = direccion * vel * Time.deltaTime;
                mov = Vector3.ClampMagnitude(mov, distancia);

                if (((Vector3.Dot(Adelante, Personaje) < 10) || (Vector3.Dot(Adelante, Personaje) > (-10))) && (PJ.position.x > (transform.position.x - 15)))
                {
                    if (distancia > 10)
                    {
                        transform.position += mov; //Lo sigue
                        an.SetFloat("Vel", 1);
                    }
                    else
                    {
                        if (muerto == false && ataquePermitido == true)
                        {
                            Invoke("Attack", 0); //Ataca
                        }
                        an.SetFloat("Vel", 0);
                    }
                }

                //Rotacion
                if (PJ.position.x > Sprite.transform.position.x)
                {
                    Sprite.transform.eulerAngles = new Vector3(-35, 180, 0);
                }
                else
                {
                    Sprite.transform.eulerAngles = new Vector3(35, 0, 0);
                }
            }
        }
        else // Cuando muere
        {
            if (an.GetBool("Muerto") == false)
            {
                Gun.SetActive(false);
                GetComponent<CapsuleCollider>().enabled = false;
                int rnd = Random.Range(0, 9);
                an.SetInteger("RandomDie", rnd);
                an.SetBool("Muerto", true);
                Invoke("Loot", 2);
            }
        }
    }

    void Attack()
    {
        ataquePermitido = false;
        an.SetBool("Attack", true);
        gunan.SetBool("Fire", true);
        Invoke("CancelarAnimacion", AttackDelay);
        Instantiate(Bullet, Gun.transform.position, Gun.transform.rotation);
    }

    void CancelarAnimacion()
    {
        an.SetBool("Attack", false);
        gunan.SetBool("Fire", false);
        ataquePermitido = true;
    }

    void Loot()
    {
        int guns = Random.Range(0, (LootGuns.Length));
        Instantiate((LootGuns[guns]), gameObject.transform.position, gameObject.transform.rotation);
    }
}
