﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VolverMenu : MonoBehaviour {

	public string menu;

	public void Volver()
	{
		SceneManager.LoadScene(menu);
	}
}
