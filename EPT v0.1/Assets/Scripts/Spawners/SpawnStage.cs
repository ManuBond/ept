﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStage : MonoBehaviour {

    public GameObject prefab;
    public float Z;

    private void OnTriggerEnter(Collider other)
    {
        // Al entrar al Trigger, se instancia un Stage a 30 metros de el, dejandolo contiguo al stage que viene
        Vector3 Posicion = new Vector3((gameObject.transform.position.x + 30), 1, Z);
        Instantiate(prefab, Posicion, new Quaternion());
    }
}
