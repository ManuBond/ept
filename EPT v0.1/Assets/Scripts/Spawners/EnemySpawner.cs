﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public Transform Player;
    public GameObject prefab;

    private void Awake()
    {
        // Al inicializarse el script va a spawnear un enemigo y luego va a volver a hacerlo cada 5 segundos
        InvokeRepeating("Spawn", 0, 5);
    }

    private void Spawn()
    {
        // El spawneo de enemigos va a hacerlos aparecer siempre 20 metros por adelante del jugador
        float X = Player.position.x + 20;
        Instantiate(prefab, new Vector3(X, 2, 0), new Quaternion());
    }
}
